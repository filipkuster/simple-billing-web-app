Rails.application.routes.draw do
  root 'profile#index'

  devise_for :users

  resources :profile, only: [:index, :edit, :update]

  resources :transactions, only: [:index, :new, :create]

  resources :users, only: [:index, :edit, :update] do
    resources :invites, only: [:create, :index, :update]
  end

  resources :friends, only: :index
end
