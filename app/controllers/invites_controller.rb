class InvitesController < ApplicationController
  def index
    @pending_invites =
      current_user
      .incoming_friend_invites
      .pending
      .includes(:sender)
      .page(params[:page])
  end

  def create
    @invite = current_user.outgoing_friend_invites.new(receiver_id: params[:user_id])
    if @invite.save
      redirect_to users_path, notice: "Friend request is sent!"
    else
      redirect_to users_path, flash: { error: @invite.errors.full_messages }
    end
  end

  def update
    @invite = current_user.incoming_friend_invites.find(params[:id])
    if @invite.update(status: params[:status])
      redirect_to user_invites_path(current_user), notice: "Friend #{params[:status]}!"
    else
      redirect_to user_invites_path(current_user), flash: { error: @invite.errors.full_messages }
    end
  end
end
