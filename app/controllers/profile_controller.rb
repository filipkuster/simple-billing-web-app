class ProfileController < ApplicationController
  def index
  end

  def edit
  end

  def update
    if current_user.update(permitted_params)
      redirect_to profile_index_path(current_user), notice: "Initial balance added!"
    else
      redirect_to edit_profile_path(current_user), flash: { error: current_user.errors.full_messages }
    end
  end

  private

  def permitted_params
    params.require(:user).permit(:current_balance)
  end
end
