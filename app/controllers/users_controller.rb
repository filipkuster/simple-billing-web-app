class UsersController < ApplicationController
  def index
    @users =
      User
      .all
      .where.not(id: current_user)
      .page(params[:page])
  end

  def edit
  end

  def update
  end
end
