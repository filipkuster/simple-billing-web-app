class TransactionsController < ApplicationController
  def index
    @transactions =
      current_user
      .all_transactions
      .page(params[:page])
  end

  def new
    @transaction = Transaction.new
  end

  def create
    transaction_creator = TransactionCreator.new(current_user, permitted_params)
    @transaction = transaction_creator.call
    if @transaction.persisted?
      redirect_to transactions_path, notice: "Transaction sucessfully created!"
    else
      flash[:error] = @transaction.errors.full_messages
      render :new
    end
  end

  private

  def permitted_params
    params.require(:transaction).permit(:receiver_id, :amount)
  end
end
