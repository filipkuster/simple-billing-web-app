class TransactionCreator
  attr_reader :errors

  def initialize(current_user, params)
    @current_user = current_user
    @params = params
  end

  def call
    ActiveRecord::Base.transaction do
      create_transaction
      raise ActiveRecord::Rollback unless @transaction.persisted?
      withdrav_balance
      deposit_balance
    end
    @transaction
  end

  private

  def create_transaction
    @transaction = @current_user.outgoing_transactions.new(@params)
    return unless @transaction.valid?
    @transaction.receiver_balance = @transaction.receiver.current_balance + @transaction.amount
    @transaction.sender_balance = @current_user.current_balance - @transaction.amount
    @transaction.save
  end

  def withdrav_balance
    @current_user.update(current_balance: @transaction.sender_balance)
  end

  def deposit_balance
    @transaction
      .receiver
      .update(current_balance: @transaction.receiver_balance)
  end
end
