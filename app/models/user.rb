class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable, :rememberable, :validatable

  has_many :outgoing_transactions, class_name: 'Transaction', foreign_key: :sender_id, dependent: :restrict_with_error
  has_many :incoming_transactions, class_name: 'Transaction', foreign_key: :receiver_id, dependent: :restrict_with_error

  has_many :outgoing_friend_invites, class_name: 'Invite', foreign_key: :sender_id, dependent: :destroy
  has_many :incoming_friend_invites, class_name: 'Invite', foreign_key: :receiver_id, dependent: :destroy

  has_many :accepted_outgoing_friend_invites, -> { where(status: :accepted)}, class_name: 'Invite', foreign_key: :sender_id
  has_many :accepted_incoming_friend_invites, -> { where(status: :accepted)}, class_name: 'Invite', foreign_key: :receiver_id

  has_many :friends_invited_by_me, through: :accepted_outgoing_friend_invites, source: :receiver, class_name: 'User'
  has_many :friends_who_invited_me, through: :accepted_incoming_friend_invites, source: :sender, class_name: 'User'

  def all_friends
    (friends_invited_by_me + friends_who_invited_me).uniq
  end

  def all_transactions
    Transaction
      .where("sender_id = :id OR receiver_id = :id", id: self.id)
      .includes(:sender, :receiver)
      .order(created_at: :desc)
  end

  def invite_sent?(friend_id)
    Invite
      .where("
        sender_id = :friend_id AND receiver_id = :id
        OR
        sender_id = :id AND receiver_id = :friend_id", friend_id: friend_id, id: self.id).present?

  end
end
