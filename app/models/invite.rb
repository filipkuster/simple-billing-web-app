class Invite < ApplicationRecord
  enum status: { pending: 0, accepted: 1, denied: 2 }

  belongs_to :sender, class_name: "User"
  belongs_to :receiver, class_name: "User"

  validates :sender, :receiver, :status, presence: true
  validates :sender, uniqueness: { scope: :receiver }
end
