class Transaction < ApplicationRecord
  belongs_to :sender, class_name: "User"
  belongs_to :receiver, class_name: "User"

  validates :amount, :sender, :receiver, presence: true

  validate :if_enough_funds?

  private

  def if_enough_funds?
    return if sender.nil?
    self.errors.add(:no_funds, 'Balance to low') if sender.current_balance < (amount || 0)
  end
end
