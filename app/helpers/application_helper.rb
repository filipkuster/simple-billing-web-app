module ApplicationHelper
  def format_datetime(datetime)
    datetime.strftime("%d/%m/%Y %H:%M")
  end

  def alert_class(type)
    case type
      when :alert then :error
      when :notice then :info
      when :error then :danger
      else type
    end
  end
end
