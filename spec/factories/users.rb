FactoryBot.define do
  factory :user do
    sequence(:email) {|n| "test_user#{n}@gmail.com" }
    password { '123456' }
    current_balance { 1000 }
  end
end
