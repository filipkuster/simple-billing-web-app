FactoryBot.define do
  factory :invite do
    association :sender, factory: :user
    association :receiver, factory: :user

    factory :pending_invite do
      status { :pending }
    end

    factory :accepted_invite do
      status { :accepted }
    end

    factory :denied_invite do
      status { :denied }
    end
  end
end
