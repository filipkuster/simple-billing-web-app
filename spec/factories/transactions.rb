FactoryBot.define do
  factory :transaction do
    association :sender, factory: :user
    association :receiver, factory: :user
    amount { 100 }
  end
end
