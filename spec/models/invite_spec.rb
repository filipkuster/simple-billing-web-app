RSpec.describe Invite, type: :model do
  describe 'associations' do
    it { should belong_to(:sender).class_name('User') }
    it { should belong_to(:receiver).class_name('User') }
  end

  describe 'types' do
    it do
      should define_enum_for(:status).
               with_values(
                 pending:  0,
                 accepted: 1,
                 denied:   2
               ).
         backed_by_column_of_type(:integer)
    end
  end

  describe 'validations' do
    it { should validate_presence_of(:sender) }
    it { should validate_presence_of(:receiver) }
    it { should validate_presence_of(:status) }
    # it { should validate_uniqueness_of(:sender).scoped_to(:receiver) }
  end
end
