RSpec.describe User, type: :model do
  describe 'associations' do
    it { should have_many(:outgoing_transactions).class_name('Transaction') }
    it { should have_many(:incoming_transactions).class_name('Transaction') }
    it { should have_many(:outgoing_friend_invites).class_name('Invite') }
    it { should have_many(:incoming_friend_invites).class_name('Invite') }
    it { should have_many(:accepted_outgoing_friend_invites).class_name('Invite') }
    it { should have_many(:accepted_incoming_friend_invites).class_name('Invite') }
    it { should have_many(:friends_invited_by_me).class_name('User') }
    it { should have_many(:friends_who_invited_me).class_name('User') }
  end

  describe 'transactions' do
    let!(:user) { create(:user) }
    let!(:transaction1) { create(:transaction, sender: user) }
    let!(:transaction2) { create(:transaction, sender: user) }
    let!(:transaction3) { create(:transaction, receiver: user) }

    it 'should return all user transactions' do
      expect(user.all_transactions.count).to eq(3)
    end

    it 'should return all incoming transactions' do
      expect(user.incoming_transactions.count).to eq(1)
    end

    it 'should return all outgoing transactions' do
      expect(user.outgoing_transactions.count).to eq(2)
    end
  end

  describe 'invites' do
    let!(:user) { create(:user) }
    let!(:invite1) { create(:pending_invite, receiver: user) }
    let!(:invite2) { create(:pending_invite, sender: user) }
    let!(:invite3) { create(:accepted_invite, sender: user) }

    it 'should return all user incoming pending invites' do
      expect(user.incoming_friend_invites.pending.count).to eq(1)
    end

    it 'should return all user outgoing pending invites' do
      expect(user.outgoing_friend_invites.pending.count).to eq(1)
    end

    it 'should return all user outgoing accepted invites' do
      expect(user.accepted_outgoing_friend_invites.count).to eq(1)
    end
  end

  describe 'friends' do
    let!(:user) { create(:user) }
    let!(:invite1) { create(:accepted_invite, receiver: user) }
    let!(:invite2) { create(:pending_invite, sender: user) }
    let!(:invite3) { create(:accepted_invite, sender: user) }

    it 'should return all user friends' do
      expect(user.all_friends.count).to eq(2)
    end
  end
end
