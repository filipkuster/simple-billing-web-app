RSpec.describe TransactionCreator do
  describe 'TransactionCreator' do
    let!(:sender) { create(:user, current_balance: 2000) }
    let!(:receiver) { create(:user, current_balance: 2000) }
    subject { TransactionCreator.new(sender, { receiver_id: receiver.id, amount: 200 }).call }


    context 'when transaction is created' do
      it 'should create new transaction' do
        expect { subject }.to change { Transaction.count }.by(1)
      end

      it 'should withdrav_balance from sender' do
        subject
        expect(sender.reload.current_balance).to eq(1800)
      end

      it 'should deposit_balance to receiver' do
        subject
        expect(receiver.reload.current_balance).to eq(2200)
      end

      it 'should take snapshot of sender balance in transaction' do
        expect(subject.reload.sender_balance).to eq(1800)
      end

      it 'should take snapshot of receiver balance in transaction' do
        expect(subject.reload.receiver_balance).to eq(2200)
      end
    end
  end
end
