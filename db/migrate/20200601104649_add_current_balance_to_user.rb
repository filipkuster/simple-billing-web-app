class AddCurrentBalanceToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :current_balance, :float
  end
end
