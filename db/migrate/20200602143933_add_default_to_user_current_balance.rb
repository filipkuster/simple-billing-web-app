class AddDefaultToUserCurrentBalance < ActiveRecord::Migration[6.0]
  def change
    change_column :users, :current_balance, :integer, default: 0
  end
end
