class AddDefaultForInviteStatus < ActiveRecord::Migration[6.0]
  def change
    change_column :invites, :status, :integer, default: 0
  end
end
