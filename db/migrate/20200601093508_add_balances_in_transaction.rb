class AddBalancesInTransaction < ActiveRecord::Migration[6.0]
  def change
    add_column :transactions, :sender_balance, :float
    add_column :transactions, :receiver_balance, :float
  end
end
